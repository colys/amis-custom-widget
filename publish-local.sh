#!/bin/bash
set -e

echo "$0";
echo "$1";

rm -rf npm
mkdir npm

cp package.json npm
cp amis.config.js npm
cp tsconfig.json npm

# npm run build2esm

cp -rf src npm

cd npm

sed -i -e 's/\"name\": \"amis-widget\"/\"name\": \"@colys\/amis-widget\"/g' ./package.json
# sed -i -e 's/\"amis-editor\":/\"@colys\/amis-editor\":/g' ./package.json
sed -i -e 's/\"amis\":/\"@colys\/amis\":/g' ./package.json

# sed -i -e "s/\'amis-editor\'/\'@colys\/amis-editor\'/g" ./amis.config.js
sed -i -e "s/\'amis\'/\'@colys\/amis\'/g" ./amis.config.js

for f in $(find ./src -name "*.ts"); do
  sed -i -e "s/from \'amis/from \'@colys\/amis/g" $f
  # sed -i -e "s/from \'amis-editor/from \'@colys\/amis-editor/g" $f
done

for f in $(find ./src -name "*.tsx"); do
  sed -i -e "s/from \'amis/from \'@colys\/amis/g" $f
  # sed -i -e "s/from \'amis-editor/from \'@colys\/amis-editor/g" $f
done

for f in $(find ./src -name "*.jsx"); do
  sed -i -e "s/from \'amis/from \'@colys\/amis/g" $f
  # sed -i -e "s/from \'amis-editor/from \'@colys\/amis-editor/g" $f
done


npm i --registry=https://registry.npmmirror.com/
amis build2lib
amis build2esm

npm publish --access public --registry=https://registry.npmjs.org/

cd ..
# rm -rf npm